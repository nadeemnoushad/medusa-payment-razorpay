"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const body_parser_1 = __importDefault(require("body-parser"));
const middlewares_1 = __importDefault(require("../../middlewares"));
const route = (0, express_1.Router)();
exports.default = (app) => {
    app.use("/razorpay", route);
    route.post("/hooks", 
    // razorpay constructEvent fails without body-parser
    body_parser_1.default.json({
        verify: (req, res, buf) => {
            req.rawBody = buf;
        },
    }), middlewares_1.default.wrap(require("./razorpay").default));
    return app;
};
