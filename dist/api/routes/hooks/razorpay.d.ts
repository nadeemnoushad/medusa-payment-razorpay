import { Request, Response } from "express";
declare module "express" {
    interface Request {
        rawBody?: any;
    }
}
declare const _default: (req: Request, res: Response) => Promise<Response<any, Record<string, any>>>;
export default _default;
