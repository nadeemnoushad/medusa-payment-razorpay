"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const hooks_1 = __importDefault(require("./routes/hooks"));
exports.default = (container) => {
    const app = (0, express_1.Router)();
    (0, hooks_1.default)(app);
    return app;
};
