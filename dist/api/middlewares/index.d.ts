declare const _default: {
    wrap: (fn: any) => (...args: any[]) => any;
};
export default _default;
