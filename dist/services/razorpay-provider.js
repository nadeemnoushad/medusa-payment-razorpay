"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const razorpay_1 = __importDefault(require("razorpay"));
const medusa_1 = require("@medusajs/medusa");
const crypto_1 = __importDefault(require("crypto"));
// import { PaymentSessionStatus } from "@medusajs/medusa/dist/models";
class RazorpayProcessor extends medusa_1.AbstractPaymentProcessor {
    constructor({ logger }, options) {
        // @ts-ignore
        // eslint-disable-next-line prefer-rest-params
        super(...arguments);
        this.logger_ = logger;
        this.options_ = options;
        this.razorpay_ = new razorpay_1.default((options = {
            key_id: options.api_key,
            key_secret: options.api_key_secret,
        }));
    }
    _validateSignature(razorpay_payment_id, razorpay_order_id, razorpay_signature) {
        let body = razorpay_order_id + "|" + razorpay_payment_id;
        let expectedSignature = crypto_1.default
            .createHmac("sha256", this.options_.api_key_secret)
            .update(body.toString())
            .digest("hex");
        return expectedSignature === razorpay_signature;
    }
    async getPaymentStatus(paymentSessionData) {
        console.log("paymentData", paymentSessionData);
        try {
            const { razorpay_order_id, razorpay_payment_id, razorpay_signature } = paymentSessionData;
            if (this._validateSignature(razorpay_payment_id, razorpay_order_id, razorpay_signature)) {
                const paymentResponse = await this.razorpay_.payments.fetch(razorpay_payment_id);
                switch (paymentResponse.status) {
                    case "created":
                        return medusa_1.PaymentSessionStatus.PENDING;
                    case "authorized":
                        return medusa_1.PaymentSessionStatus.AUTHORIZED;
                    case "captured":
                        return medusa_1.PaymentSessionStatus.AUTHORIZED;
                    case "refunded":
                        return medusa_1.PaymentSessionStatus.AUTHORIZED;
                    case "failed":
                        return medusa_1.PaymentSessionStatus.ERROR;
                    default:
                        return medusa_1.PaymentSessionStatus.PENDING;
                }
            }
        }
        catch (error) {
            throw error;
        }
    }
    async createCustomer(customer) {
        var _a, _b, _c, _d, _e;
        try {
            let createCustomerQueryParams = {
                fail_existing: 0,
                email: "startup@medusa.com",
            };
            let razorpayCustomer = undefined;
            let razorpayCustomerUpdated = undefined;
            let fullname = ((_a = customer.first_name) !== null && _a !== void 0 ? _a : "") + " " + ((_b = customer.last_name) !== null && _b !== void 0 ? _b : "");
            let customerName = (_c = customer.name) !== null && _c !== void 0 ? _c : fullname;
            let notes = {};
            if ((customerName === null || customerName === void 0 ? void 0 : customerName.length) > RazorpayProcessor.RAZORPAY_NAME_LENGTH_LIMIT ||
                customerName === " ") {
                createCustomerQueryParams["name"] = customerName === null || customerName === void 0 ? void 0 : customerName.substring(0, 50);
            }
            else {
                createCustomerQueryParams["name"] = customerName !== null && customerName !== void 0 ? customerName : "medusa-startup";
            }
            createCustomerQueryParams["notes"] = { fullname: customerName };
            //  if (customer.email !=undefined )  {
            createCustomerQueryParams.email = (_d = customer.email) !== null && _d !== void 0 ? _d : "startup@medusa.com";
            // }
            //  if (customer.contact !=undefined ) {
            createCustomerQueryParams["contact"] = (_e = customer.contact) !== null && _e !== void 0 ? _e : "9000000000";
            // }
            createCustomerQueryParams["notes"]["customer_id"] = customer.id;
            try {
                razorpayCustomer = await this.razorpay_.customers.create(createCustomerQueryParams);
                if (customer.id) {
                    await this.customerService_.update(customer.id, {
                        metadata: { razorpay_id: razorpayCustomer.id },
                    });
                }
            }
            catch (error) {
                razorpayCustomer = this._findExistingCustomer(customer.email, customer.contact);
            }
            if (razorpayCustomer === null || razorpayCustomer === void 0 ? void 0 : razorpayCustomer.created_at) {
                razorpayCustomerUpdated = await this.updateCustomer(razorpayCustomer.id, customer); /* updating the remaining details */
            }
            return razorpayCustomerUpdated !== null && razorpayCustomerUpdated !== void 0 ? razorpayCustomerUpdated : razorpayCustomer;
        }
        catch (error) {
            throw error;
        }
    }
    async initiatePayment(cart) {
        var _a;
        const { customer_id, region_id, email, order_number, display_id } = cart;
        const { currency_code } = await this.regionService_.retrieve(region_id);
        const amount = await this.totalsService_.getTotal(cart);
        const intentRequest = {
            amount: amount,
            currency: currency_code.toString().toUpperCase(),
            receipt: (display_id !== null && display_id !== void 0 ? display_id : "0000") + "_seq_" + RazorpayProcessor.seq_number,
            // partial_payment:true,
            notes: { cart_id: `${cart.id}` },
        };
        RazorpayProcessor.seq_number = RazorpayProcessor.seq_number + 1;
        if (customer_id) {
            const customer = await this.customerService_.retrieve(customer_id);
            if ((_a = customer.metadata) === null || _a === void 0 ? void 0 : _a.razorpay_id) {
                intentRequest.notes["customer"] = customer.metadata.razorpay_id;
            }
            else {
                const razorpayCustomer = await this.createCustomer({
                    email,
                    id: customer_id,
                    name: "unknown",
                });
                intentRequest.notes["customer_id"] = razorpayCustomer.id;
            }
        }
        else {
            const razorpayCustomer = await this.createCustomer({
                email,
                name: "unknown",
            });
            intentRequest.notes["customer_id"] = razorpayCustomer.id;
        }
        const orderIntent = await this.razorpay_.orders.create(intentRequest);
        return orderIntent;
    }
    async capturePayment(paymentData) {
        try {
            const { razorpay_payment_id, razorpay_order_id, razorpay_signature } = paymentData.data["notes"];
            if (!this._validateSignature(razorpay_payment_id, razorpay_order_id, razorpay_signature))
                return;
            const paymentIntent = await this.razorpay_.payments.fetch(razorpay_payment_id);
            if (paymentIntent.status === "captured") {
                return paymentIntent;
            }
            else {
                return await this.razorpay_.payments.capture(razorpay_payment_id, paymentIntent.amount, paymentIntent.currency);
            }
        }
        catch (error) {
            if (error.code === "payment_intent_unexpected_state") {
                if (error.payment_intent.status === "succeeded") {
                    return error.payment_intent;
                }
            }
            throw error;
        }
    }
    async authorizePayment(paymentSessionData, context) {
        const stat = await this.getPaymentStatus(paymentSessionData);
        try {
            return { data: paymentSessionData, status: stat };
        }
        catch (error) {
            throw error;
        }
    }
    async cancelPayment(payment) {
        const { id } = payment.data;
        try {
            return await this.razorpay_.orders.fetch(id);
        }
        catch (error) {
            if (error.payment_intent.status === "canceled") {
                return error.payment_intent;
            }
            throw error;
        }
    }
    async deletePayment(paymentData) {
        throw new Error("Razorpay doesnot support deleting orders/payments.");
    }
    async refundPayment(paymentData, refundAmount) {
        const orderInformation = await this.razorpay_.orders.fetch(paymentData.data.order_id);
        const { razorpay_payment_id, razorpay_order_id, razorpay_signature } = orderInformation.notes;
        if (!this._validateSignature(razorpay_payment_id, razorpay_order_id, razorpay_signature))
            return;
        try {
            let paymentMade = await this.razorpay_.payments.fetch(razorpay_payment_id);
            if (paymentMade.amount - paymentMade.amount_refunded >= refundAmount) {
                const refundResult = await this.razorpay_.payments.refund(razorpay_payment_id, {
                    amount: Math.round(refundAmount),
                    // id: razorpay_payment_id,
                    receipt: paymentData.data.order_id,
                });
                return refundResult;
            }
            else
                return;
        }
        catch (error) {
            throw error;
        }
    }
    async retrievePayment(paymentSessionData) {
        try {
            return this.razorpay_.orders.fetch(paymentSessionData.id);
        }
        catch (error) {
            throw error;
        }
    }
    async updatePayment(context) {
        try {
            return this.createPayment(context);
        }
        catch (error) {
            throw error;
        }
    }
    async updatePaymentData(sessionData, data) {
        try {
            let result = {};
            if (data.razorpay_payment_id) {
                result = this.razorpay_.orders.edit(sessionData.id, {
                    notes: {
                        razorpay_payment_id: data.razorpay_payment_id,
                        razorpay_order_id: data.razorpay_order_id,
                        razorpay_signature: data.razorpay_signature,
                    },
                });
            }
            else {
                result = this.razorpay_.orders.edit(sessionData.id, {
                    notes: data,
                });
            }
            return result;
        }
        catch (error) {
            throw error;
        }
    }
    constructWebhookEvent(data, signature) {
        return this.razorpay_.webhooks.constructEvent(data, signature, this.options_.webhook_secret);
    }
}
RazorpayProcessor.identifier = "razorpay";
RazorpayProcessor.seq_number = 0;
RazorpayProcessor.RAZORPAY_NAME_LENGTH_LIMIT = 50;
exports.default = RazorpayProcessor;
