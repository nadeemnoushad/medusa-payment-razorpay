import { AbstractPaymentProcessor, PaymentSessionStatus } from "@medusajs/medusa";
import { Logger } from "@medusajs/types";
import { PaymentProcessorError, PaymentProcessorContext, PaymentProcessorSessionResponse } from "@medusajs/medusa/dist/interfaces";
declare class RazorpayProcessor extends AbstractPaymentProcessor {
    [x: string]: any;
    static identifier: string;
    static seq_number: number;
    static RAZORPAY_NAME_LENGTH_LIMIT: number;
    constructor({ logger }: {
        logger?: Logger;
    }, options: any);
    _validateSignature(razorpay_payment_id: any, razorpay_order_id: any, razorpay_signature: any): boolean;
    getPaymentStatus(paymentSessionData: Record<string, unknown>): Promise<PaymentSessionStatus>;
    createCustomer(customer: any): Promise<any>;
    initiatePayment(cart: any): Promise<any>;
    capturePayment(paymentData: Record<string, unknown>): Promise<Record<string, unknown> | PaymentProcessorError>;
    authorizePayment(paymentSessionData: Record<string, unknown>, context: Record<string, unknown>): Promise<PaymentProcessorError | {
        status: PaymentSessionStatus;
        data: Record<string, unknown>;
    }>;
    cancelPayment(payment: any): Promise<Record<string, unknown> | PaymentProcessorError>;
    deletePayment(paymentData: Record<string, unknown>): Promise<Record<string, unknown> | PaymentProcessorError>;
    refundPayment(paymentData: any, refundAmount: number): Promise<Record<string, unknown> | PaymentProcessorError>;
    retrievePayment(paymentSessionData: Record<string, unknown>): Promise<Record<string, unknown> | PaymentProcessorError>;
    updatePayment(context: PaymentProcessorContext): Promise<void | PaymentProcessorError | PaymentProcessorSessionResponse>;
    updatePaymentData(sessionData: any, data: Record<string, unknown>): Promise<Record<string, unknown> | PaymentProcessorError>;
    constructWebhookEvent(data: any, signature: any): any;
}
export default RazorpayProcessor;
