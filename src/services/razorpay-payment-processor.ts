import Razorpay from "razorpay";
import { EOL } from "os";
import {
  AbstractPaymentProcessor,
  isPaymentProcessorError,
  PaymentProcessorContext,
  PaymentProcessorError,
  PaymentProcessorSessionResponse,
  PaymentSessionStatus,
} from "@medusajs/medusa";
import {
  ErrorCodes,
  ErrorIntentStatus,
  PaymentIntentOptions,
  StripeOptions,
} from "../types";
import { MedusaError } from "@medusajs/utils";

class RazorpayProcessor extends AbstractPaymentProcessor {
  static identifier = "";

  protected readonly options_: StripeOptions;
  protected razorpay_: Razorpay;
  webhooks: any;
  [x: string]: any;

  protected constructor(_, options) {
    super(_, options);

    this.options_ = options;

    // this.razorpay_ = new Razorpay({});
    this.razorpay_ = new Razorpay({
      key_id: options.api_key,
      key_secret: options.api_key_secret,
    });
  }

  //  get paymentIntentOptions(): PaymentIntentOptions;

  getPaymentIntentOptions(): PaymentIntentOptions {
    const options: PaymentIntentOptions = {};

    if (this?.paymentIntentOptions?.capture_method) {
      options.capture_method = this.paymentIntentOptions.capture_method;
    }

    if (this?.paymentIntentOptions?.setup_future_usage) {
      options.setup_future_usage = this.paymentIntentOptions.setup_future_usage;
    }

    if (this?.paymentIntentOptions?.payment_method_types) {
      options.payment_method_types =
        this.paymentIntentOptions.payment_method_types;
    }

    return options;
  }

  // similar to rp format
  async getPaymentStatus(
    paymentSessionData: Record<string, unknown>
  ): Promise<PaymentSessionStatus> {
    const id = paymentSessionData.id as string;

    const paymentResponse = await this.razorpay_.payments.fetch(id);

    switch (paymentResponse.status) {
      case "created":
        return PaymentSessionStatus.PENDING;

      case "authorized":
        return PaymentSessionStatus.AUTHORIZED;

      case "captured":
        return PaymentSessionStatus.AUTHORIZED;

      case "refunded":
        return PaymentSessionStatus.AUTHORIZED;

      case "failed":
        return PaymentSessionStatus.ERROR;

      default:
        return PaymentSessionStatus.PENDING;
    }
  }

  // idk about this
  async initiatePayment(context: PaymentProcessorContext) {
    const intentRequestData = this.getPaymentIntentOptions();
    const {
      email,
      context: cart_context,
      currency_code,
      amount,
      resource_id,
      customer,
    } = context;

    const description = (cart_context.payment_description ??
      this.options_?.payment_description) as string;

    const intentRequest = {
      description,
      amount: Math.round(amount),
      currency: currency_code,
      metadata: { resource_id },
      capture_method: this.options_.capture ? "automatic" : "manual",
      ...intentRequestData,
    };

    if (customer?.metadata?.razorpay_id) {
      intentRequest["customer"] = customer.metadata.razorpay_id as string;
    } else {
      let stripeCustomer;
      try {
        stripeCustomer = await this.razorpay_.customers.create({
          email,
          name: "test",
        });
      } catch (e) {
        return this.buildError(
          "An error occurred in initiatePayment when creating a Razorpay customer",
          e
        );
      }

      intentRequest["customer"] = stripeCustomer.id;
    }

    return {
      update_requests: customer?.metadata?.razorpay_id
        ? undefined
        : {
            customer_metadata: {
              razorpay_id: intentRequest["customer"],
            },
          },
    };
  }

  async authorizePayment(
    paymentSessionData: Record<string, unknown>,
    context: Record<string, unknown>
  ): Promise<
    | PaymentProcessorError
    | {
        status: PaymentSessionStatus;
        data: PaymentProcessorSessionResponse["session_data"];
      }
  > {
    const status = await this.getPaymentStatus(paymentSessionData);
    return { data: paymentSessionData, status };
  }

  // Similar to rp format // check id
  async cancelPayment(paymentSessionData: Record<string, unknown>) {
    try {
      const id = paymentSessionData.id as string;
      return await this.razorpay_.orders.fetch(id);
    } catch (error) {
      if (error.payment_intent?.status === ErrorIntentStatus.CANCELED) {
        return error.payment_intent;
      }

      return this.buildError("An error occurred in cancelPayment", error);
    }
  }

  // Somewhat similar to rp format // check id
  async capturePayment(paymentSessionData: Record<string, unknown>) {
    const id = paymentSessionData.id as string;
    try {
      // const { } = paymentSessionData.data.notes
      const paymentIntent = await this.razorpay_.payments.fetch(id);

      if (paymentIntent.status === "captured") {
        return paymentIntent;
      }
    } catch (error) {
      return this.buildError("An error occurred in capturePayment", error);
    }
  }

  // RP doesnot support deleting order/payments
  async deletePayment(
    paymentSessionData: Record<string, unknown>
  ): Promise<
    PaymentProcessorError | PaymentProcessorSessionResponse["session_data"]
  > {
    return await this.cancelPayment(paymentSessionData);
  }

  // similar to rp format
  async refundPayment(
    paymentSessionData: Record<string, unknown>,
    refundAmount: number
  ) {
    const id = paymentSessionData.id as string;
    const orderInformation = await this.razorpay_.orders.fetch(id);
    const { razorpay_payment_id, razorpay_order_id, razorpay_signature } =
      orderInformation.notes as any;
    try {
      let paymentMade = (await this.razorpay_.payments.fetch(
        razorpay_payment_id
      )) as any;
      if (paymentMade.amount - paymentMade.amount_refunded >= refundAmount) {
        const refundResult = await this.razorpay_.payments.refund(
          razorpay_payment_id,
          {
            amount: Math.round(refundAmount),
            speed: "optimum",
            receipt: "rcpt_1",
          }
        );
        return refundResult;
      }
    } catch (e) {
      return this.buildError("An error occurred in refundPayment", e);
    }

    return paymentSessionData;
  }

  // Similar to rp format
  async retrievePayment(paymentSessionData) {
    try {
      return this.razorpay_.orders.fetch(paymentSessionData);
    } catch (e) {
      return this.buildError("An error occurred in retrievePayment", e);
    }
  }

  // Somewhat similar to rp format
  async updatePayment(
    context: PaymentProcessorContext
  ): Promise<PaymentProcessorError | PaymentProcessorSessionResponse | void> {
    const { amount, customer, paymentSessionData } = context;
    const RazorpayId = customer?.metadata?.razorpay_id;

    if (RazorpayId !== paymentSessionData.customer) {
      const result = await this.initiatePayment(context);
      if (isPaymentProcessorError(result)) {
        return this.buildError(
          "An error occurred in updatePayment during the initiate of the new payment for the new customer",
          result
        );
      }
      return result;
    } else {
      return this.buildError("An error occurred in updatePayment");
    }
  }

  // MADE IT TO RAZORPAY FORMAT
  async updatePaymentData(sessionId: string, data: Record<string, unknown>) {
    try {
      // Prevent from updating the amount from here as it should go through
      // the updatePayment method to perform the correct logic
      if (data.amount) {
        throw new MedusaError(
          MedusaError.Types.INVALID_DATA,
          "Cannot update amount, use updatePayment instead"
        );
      }
      if (data.razorpay_payment_id) {
        return await this.razorpay_.orders.edit(sessionId, {
          notes: {
            razorpay_payment_id: data.razorpay_payment_id,
            razorpay_order_id: data.razorpay_order_id,
            razorpay_signature: data.razorpay_signature,
          } as any,
        });
      } else {
        return await this.razorpay_.orders.edit(sessionId, {
          notes: data as any,
        });
      }
    } catch (e) {
      return this.buildError("An error occurred in updatePaymentData", e);
    }
  }

  // TODO: CONSTRUCT WEBHOOK
  /**
   * Constructs Stripe Webhook event
   * @param {object} data - the data of the webhook request: req.body
   * @param {object} signature - the Stripe signature on the event, that
   *    ensures integrity of the webhook event
   * @return {object} Stripe Webhook event
   */
  // constructWebhookEvent(data, signature) {
  //   return this.razorpay_.webhooks.constructEvent(
  //     data,
  //     signature,
  //     this.options_.webhook_secret
  //   );
  // }
}

export default RazorpayProcessor;
