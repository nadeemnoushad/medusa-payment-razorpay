import { PaymentService } from "medusa-interfaces";
import Razorpay from "razorpay";
import rp from "razorpay-node-typescript";

import {
  AbstractPaymentProcessor,
  PaymentSessionData,
  PaymentSessionStatus,
} from "@medusajs/medusa";
import crypto from "crypto";
import { Logger } from "@medusajs/types";
import {
  PaymentProcessorError,
  PaymentProcessorContext,
  PaymentProcessorSessionResponse,
} from "@medusajs/medusa/dist/interfaces";
// import { PaymentSessionStatus } from "@medusajs/medusa/dist/models";

class RazorpayProcessor extends AbstractPaymentProcessor {
  [x: string]: any;
  static identifier = "razorpay";
  static seq_number = 0;
  static RAZORPAY_NAME_LENGTH_LIMIT = 50;

  constructor({ logger }: { logger?: Logger }, options) {
    // @ts-ignore
    // eslint-disable-next-line prefer-rest-params
    super(...arguments);

    this.logger_ = logger;
    this.options_ = options;

    this.razorpay_ = new Razorpay(
      (options = {
        key_id: options.api_key,
        key_secret: options.api_key_secret,
      })
    );
  }

  _validateSignature(
    razorpay_payment_id,
    razorpay_order_id,
    razorpay_signature
  ) {
    let body = razorpay_order_id + "|" + razorpay_payment_id;
    let expectedSignature = crypto
      .createHmac("sha256", this.options_.api_key_secret)
      .update(body.toString())
      .digest("hex");
    return expectedSignature === razorpay_signature;
  }

  async getPaymentStatus(
    paymentSessionData: Record<string, unknown>
  ): Promise<PaymentSessionStatus> {
    console.log("paymentData", paymentSessionData);

    try {
      const { razorpay_order_id, razorpay_payment_id, razorpay_signature } =
        paymentSessionData;

      if (
        this._validateSignature(
          razorpay_payment_id,
          razorpay_order_id,
          razorpay_signature
        )
      ) {
        const paymentResponse = await this.razorpay_.payments.fetch(
          razorpay_payment_id
        );

        switch (paymentResponse.status) {
          case "created":
            return PaymentSessionStatus.PENDING;

          case "authorized":
            return PaymentSessionStatus.AUTHORIZED;

          case "captured":
            return PaymentSessionStatus.AUTHORIZED;

          case "refunded":
            return PaymentSessionStatus.AUTHORIZED;

          case "failed":
            return PaymentSessionStatus.ERROR;

          default:
            return PaymentSessionStatus.PENDING;
        }
      }
    } catch (error) {
      throw error;
    }
  }

  async createCustomer(customer) {
    try {
      let createCustomerQueryParams = {
        fail_existing: 0,
        email: "startup@medusa.com",
      };
      let razorpayCustomer = undefined;
      let razorpayCustomerUpdated = undefined;
      let fullname =
        (customer.first_name ?? "") + " " + (customer.last_name ?? "");
      let customerName = customer.name ?? fullname;
      let notes = {};
      if (
        customerName?.length > RazorpayProcessor.RAZORPAY_NAME_LENGTH_LIMIT ||
        customerName === " "
      ) {
        createCustomerQueryParams["name"] = customerName?.substring(0, 50);
      } else {
        createCustomerQueryParams["name"] = customerName ?? "medusa-startup";
      }
      createCustomerQueryParams["notes"] = { fullname: customerName };
      //  if (customer.email !=undefined )  {
      createCustomerQueryParams.email = customer.email ?? "startup@medusa.com";
      // }
      //  if (customer.contact !=undefined ) {
      createCustomerQueryParams["contact"] = customer.contact ?? "9000000000";
      // }
      createCustomerQueryParams["notes"]["customer_id"] = customer.id;
      try {
        razorpayCustomer = await this.razorpay_.customers.create(
          createCustomerQueryParams
        );
        if (customer.id) {
          await this.customerService_.update(customer.id, {
            metadata: { razorpay_id: razorpayCustomer.id },
          });
        }
      } catch (error) {
        razorpayCustomer = this._findExistingCustomer(
          customer.email,
          customer.contact
        );
      }
      if (razorpayCustomer?.created_at) {
        razorpayCustomerUpdated = await this.updateCustomer(
          razorpayCustomer.id,
          customer
        ); /* updating the remaining details */
      }

      return razorpayCustomerUpdated ?? razorpayCustomer;
    } catch (error) {
      throw error;
    }
  }

  async initiatePayment(cart) {
    const { customer_id, region_id, email, order_number, display_id } = cart;
    const { currency_code } = await this.regionService_.retrieve(region_id);

    const amount = await this.totalsService_.getTotal(cart);

    const intentRequest = {
      amount: amount,
      currency: currency_code.toString().toUpperCase(),
      receipt: (display_id ?? "0000") + "_seq_" + RazorpayProcessor.seq_number,
      // partial_payment:true,
      notes: { cart_id: `${cart.id}` },
    };
    RazorpayProcessor.seq_number = RazorpayProcessor.seq_number + 1;
    if (customer_id) {
      const customer = await this.customerService_.retrieve(customer_id);

      if (customer.metadata?.razorpay_id) {
        intentRequest.notes["customer"] = customer.metadata.razorpay_id;
      } else {
        const razorpayCustomer = await this.createCustomer({
          email,
          id: customer_id,
          name: "unknown",
        });

        intentRequest.notes["customer_id"] = razorpayCustomer.id;
      }
    } else {
      const razorpayCustomer = await this.createCustomer({
        email,
        name: "unknown",
      });

      intentRequest.notes["customer_id"] = razorpayCustomer.id;
    }

    const orderIntent = await this.razorpay_.orders.create(intentRequest);

    return orderIntent;
  }

  async capturePayment(
    paymentData: Record<string, unknown>
  ): Promise<Record<string, unknown> | PaymentProcessorError> {
    try {
      const { razorpay_payment_id, razorpay_order_id, razorpay_signature } =
        paymentData.data["notes"];
      if (
        !this._validateSignature(
          razorpay_payment_id,
          razorpay_order_id,
          razorpay_signature
        )
      )
        return;
      const paymentIntent = await this.razorpay_.payments.fetch(
        razorpay_payment_id
      );
      if (paymentIntent.status === "captured") {
        return paymentIntent;
      } else {
        return await this.razorpay_.payments.capture(
          razorpay_payment_id,
          paymentIntent.amount,
          paymentIntent.currency
        );
      }
    } catch (error) {
      if (error.code === "payment_intent_unexpected_state") {
        if (error.payment_intent.status === "succeeded") {
          return error.payment_intent;
        }
      }
      throw error;
    }
  }

  async authorizePayment(
    paymentSessionData: Record<string, unknown>,
    context: Record<string, unknown>
  ): Promise<
    | PaymentProcessorError
    | { status: PaymentSessionStatus; data: Record<string, unknown> }
  > {
    const stat = await this.getPaymentStatus(paymentSessionData);

    try {
      return { data: paymentSessionData, status: stat };
    } catch (error) {
      throw error;
    }
  }
  async cancelPayment(
    payment
  ): Promise<Record<string, unknown> | PaymentProcessorError> {
    const { id } = payment.data;
    try {
      return await this.razorpay_.orders.fetch(id);
    } catch (error) {
      if (error.payment_intent.status === "canceled") {
        return error.payment_intent;
      }

      throw error;
    }
  }
  async deletePayment(
    paymentData: Record<string, unknown>
  ): Promise<Record<string, unknown> | PaymentProcessorError> {
    throw new Error("Razorpay doesnot support deleting orders/payments.");
  }
  async refundPayment(
    paymentData,
    refundAmount: number
  ): Promise<Record<string, unknown> | PaymentProcessorError> {
    const orderInformation = await this.razorpay_.orders.fetch(
      paymentData.data.order_id
    );
    const { razorpay_payment_id, razorpay_order_id, razorpay_signature } =
      orderInformation.notes;
    if (
      !this._validateSignature(
        razorpay_payment_id,
        razorpay_order_id,
        razorpay_signature
      )
    )
      return;
    try {
      let paymentMade = await this.razorpay_.payments.fetch(
        razorpay_payment_id
      );
      if (paymentMade.amount - paymentMade.amount_refunded >= refundAmount) {
        const refundResult = await this.razorpay_.payments.refund(
          razorpay_payment_id,
          {
            amount: Math.round(refundAmount),
            // id: razorpay_payment_id,
            receipt: paymentData.data.order_id,
          }
        );
        return refundResult;
      } else return;
    } catch (error) {
      throw error;
    }
  }
  async retrievePayment(
    paymentSessionData: Record<string, unknown>
  ): Promise<Record<string, unknown> | PaymentProcessorError> {
    try {
      return this.razorpay_.orders.fetch(paymentSessionData.id);
    } catch (error) {
      throw error;
    }
  }
  async updatePayment(
    context: PaymentProcessorContext
  ): Promise<void | PaymentProcessorError | PaymentProcessorSessionResponse> {
    try {
      return this.createPayment(context);
    } catch (error) {
      throw error;
    }
  }

  async updatePaymentData(
    sessionData,
    data: Record<string, unknown>
  ): Promise<Record<string, unknown> | PaymentProcessorError> {
    try {
      let result = {};

      if (data.razorpay_payment_id) {
        result = this.razorpay_.orders.edit(sessionData.id, {
          notes: {
            razorpay_payment_id: data.razorpay_payment_id,
            razorpay_order_id: data.razorpay_order_id,
            razorpay_signature: data.razorpay_signature,
          },
        });
      } else {
        result = this.razorpay_.orders.edit(sessionData.id, {
          notes: data,
        });
      }

      return result;
    } catch (error) {
      throw error;
    }
  }

  constructWebhookEvent(data, signature) {
    return this.razorpay_.webhooks.constructEvent(
      data,
      signature,
      this.options_.webhook_secret
    );
  }
}

export default RazorpayProcessor;
